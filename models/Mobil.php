<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mobil".
 *
 * @property int $id
 * @property int $merk_id
 * @property string $nama
 * @property string $foto
 * @property int $is_delete
 *
 * @property Merk $merk
 */
class Mobil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mobil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['merk_id', 'nama', 'is_delete'], 'required'],
            [['merk_id', 'is_delete'], 'integer'],
            [['foto'], 'string'],
            [['nama'], 'string', 'max' => 100],
            [['merk_id'], 'exist', 'skipOnError' => true, 'targetClass' => Merk::className(), 'targetAttribute' => ['merk_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'merk_id' => 'Merk ID',
            'nama' => 'Nama',
            'foto' => 'Foto',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerk()
    {
        return $this->hasOne(Merk::className(), ['id' => 'merk_id']);
    }
}
