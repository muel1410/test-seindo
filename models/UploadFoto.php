<?php  
    namespace app\models;

    use yii\base\Model;
    use yii\web\UploadedFile;

    /**
     * UploadForm is the model behind the upload form.
     */
    class UploadFoto extends Model
    {
        /**
         * @var UploadedFile file attribute
         */
        public $file;

        /**
         * @return array the validation rules.
         */
        public function rules()
        {
            return [
                [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 50]
            ];
        }
    }
?>