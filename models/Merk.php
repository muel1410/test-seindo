<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merk".
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property int $is_delete
 *
 * @property Mobil[] $mobils
 */
class Merk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'merk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode', 'nama', 'is_delete'], 'required'],
            [['is_delete'], 'integer'],
            [['kode'], 'string', 'max' => 20],
            [['nama'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMobils()
    {
        return $this->hasMany(Mobil::className(), ['merk_id' => 'id']);
    }
}
