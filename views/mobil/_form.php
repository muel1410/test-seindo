<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Mobil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mobil-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $form->field($model, 'merk_id')->dropDownList($DataMerk, ['prompt' => 'Pilih Merk...', 'id' => 'datamerk'])->label('Merk') ?>

    <?php echo $form->field($model, 'nama')->textInput() ?>

    <?= $form->field($Foto, 'file')->fileInput()->Label('Foto') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
